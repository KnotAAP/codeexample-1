package com.vlc.slimbk.data_model

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log

import org.chromium.net.CronetEngine
import org.chromium.net.UploadDataProvider
import org.chromium.net.UploadDataProviders
import org.chromium.net.UrlRequest
import org.json.JSONObject
import java.util.*
import java.util.concurrent.Executor
import java.util.concurrent.Executors
import kotlin.collections.HashMap

class VLHttpConnect private constructor(private val context: Context) {
    enum class VLAuthType {
        AuthDefault,
        AuthBearer,
    }

    enum class VLHttpMethodType(val type: String) {
        GET("GET"),
        POST("POST"),
        PUT("PUT"),
        DELETE("DELETE"),
    }

    enum class VLContentType(val type: String) {
        ContentVoid(""),
        ContentApplicationXWwwFormUrlencoded("application/x-www-form-urlencoded"),
        ContentApplicationJson("application/json"),
        ContentMultipartFormData("multipart/form-data"),
    }

    private var cronetEngine: CronetEngine? = null
    var executor: Executor? = null

    private var token: String = ""

    companion object {
        @SuppressLint("StaticFieldLeak")
        @Volatile
        private var instance: VLHttpConnect? = null

        fun getInstance(context: Context): VLHttpConnect? {
            var localInstance = instance

            if (localInstance == null) {
                localInstance = instance

                if (localInstance == null) {
                    localInstance = VLHttpConnect(context)

                    localInstance?.getCronetEngine(context)

                    instance = localInstance
                }
            }
            return localInstance
        }
    }

    @Synchronized
    private fun getCronetEngine(context: Context) {
        // Lazily create the Cronet engine.
        if (cronetEngine == null) {
            val builder = CronetEngine.Builder(context)

            // Enable caching of HTTP data and
            // other information like QUIC server information, HTTP/2 protocol.

            cronetEngine = builder
                .enableHttpCache(CronetEngine.Builder.HTTP_CACHE_IN_MEMORY, (1000 * 1024).toLong())
                .build()

            executor = Executors.newFixedThreadPool(3)
        }
    }

    fun removeToken() {
        addToken("")
    }

    fun addToken(token: String?) {
        this.token = token ?: ""
    }

    fun loadRequest(
        methodType: VLHttpMethodType,
        contentType: VLContentType,
        authType: VLAuthType,
        path: String,
        queryData: HashMap<String, Any>?,
        bodyData: Any?,
        successListener: VLSuccessUrlRequestCallbackListener,
        errorListener: VLErrorUrlRequestCallbackListener
    ) {
        var path = path

        if (queryData?.count()!! > 0) {
            path = getUrlParams(path, queryData)
        }

        var uploadDataProvider: UploadDataProvider = UploadDataProviders.create(byteArrayOf())

        var requestBuilder = cronetEngine!!.newUrlRequestBuilder(
            path,
            VLUrlRequestCallback(successListener, errorListener),
            executor
        )

        requestBuilder.setPriority(0)

        requestBuilder.setHttpMethod(methodType.type)
        requestBuilder.addHeader("Content-Type", contentType.type)

        Log.i("URL: ", path)
        Log.i("Content-Type: ", contentType.type)
        Log.i("Http-Method: ", methodType.type)

        var authorization = ""

        if (token != null &&
            token != ""
        ) {
            when (authType) {
                VLAuthType.AuthDefault -> {
                    authorization = token
                }

                VLAuthType.AuthBearer -> {
                    authorization = "Bearer $token"
                }
            }

            requestBuilder.addHeader("Authorization", authorization)
        }

        Log.i("Authorization", authorization)

        when (contentType) {
            VLContentType.ContentVoid -> {

            }

            VLContentType.ContentApplicationXWwwFormUrlencoded -> {
                requestBuilder.addHeader("Content-Type", "application/x-www-form-urlencoded")
            }

            VLContentType.ContentApplicationJson -> {
                requestBuilder.addHeader("Content-Type", "application/json")
            }

            VLContentType.ContentMultipartFormData -> {

            }
        }

        Log.i("HttpConnect", requestBuilder.toString())

        when (contentType) {
            VLContentType.ContentVoid -> {

            }

            VLContentType.ContentApplicationXWwwFormUrlencoded -> {
                if (bodyData != null) {
                    uploadDataProvider =
                        getBodyForXWwwFormUrlEncoded(bodyData as HashMap<String, String>)
                }
            }

            VLContentType.ContentApplicationJson -> {
                if (bodyData != null) {
                    uploadDataProvider = getBodyForJsonEncoded(
                        bodyData as MutableMap<String, Any>
                    )
                }
            }

            VLContentType.ContentMultipartFormData -> {

            }
        }

        requestBuilder.setUploadDataProvider(
            uploadDataProvider,
            executor
        )

        val request: UrlRequest = requestBuilder.build()

        request.start()
    }

    fun getUrlParams(path: String, paramMap: HashMap<String, Any>): String {
        if (paramMap.keys.count() == 0) {
            return path
        }

        var pathParam = ""

        for (key in paramMap.keys) {
            if (paramMap[key] is ArrayList<*>) {
                val queryList = (paramMap[key] as ArrayList<String>)

                for (item in queryList) {
                    pathParam = "$pathParam$key[]=$item"

                    if (item != queryList.last()) pathParam += "&"
                }
            } else {
                pathParam = pathParam + key + "=" + paramMap[key].toString()
            }

            if (key != paramMap.keys.last()) pathParam += "&"
        }

        return "$path?$pathParam"
    }

    private fun getBodyForXWwwFormUrlEncoded(bodyMap: HashMap<String, String>): UploadDataProvider {
        val charset = Charsets.UTF_8
        var bodyStr = ""
        var body = ByteArray(0)
        var value = ""

        for (key in bodyMap.keys) {
            value = bodyMap[key].toString()

            bodyStr = if (key != bodyMap.keys.last()) {
                "$bodyStr$key=$value&"
                //bodyStr + key + "=" + URLEncoder.encode(value) + "&"
            } else {
                "$bodyStr$key=$value"
                //bodyStr + key + "=" + URLEncoder.encode(value)
            }
        }

        body = bodyStr.toByteArray(charset)

        Log.i("XWwwFormUrl: ", bodyStr)

        return UploadDataProviders.create(body)
    }

    private fun getBodyForJsonEncoded(bodyMap: MutableMap<String, Any>): UploadDataProvider {
        val charset = Charsets.UTF_8
        var bodyStr = ""
        var body = ByteArray(0)

        bodyStr = JSONObject(bodyMap as Map<Any, Any>).toString()

        body = bodyStr.toByteArray(charset)

        Log.i("JSON: ", bodyStr)

        return UploadDataProviders.create(body)
    }
}