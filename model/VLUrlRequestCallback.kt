package com.vlc.slimbk.data_model

import android.util.Log
import org.chromium.net.CronetException
import org.chromium.net.UrlRequest
import org.chromium.net.UrlResponseInfo
import org.json.JSONException
import org.json.JSONObject
import java.nio.ByteBuffer
import java.nio.charset.Charset

private const val TAG = "VLUrlRequestCallback"

interface VLUrlRequestCallbackListener {
    fun SuccessDefaultHandler(obj: Any)
    fun ErrorDefaultHandler(message: String, error: Error)
}

interface VLSuccessUrlRequestCallbackListener {
    fun SuccessDefaultHandler(obj: Any)
}

interface VLErrorUrlRequestCallbackListener {
    fun ErrorDefaultHandler(message: String, error: Error, statusCode: Int, response: JSONObject)
}

class VLUrlRequestCallback(
    successListener: VLSuccessUrlRequestCallbackListener,
    errorListener: VLErrorUrlRequestCallbackListener
) : UrlRequest.Callback() {
    private var buffer = ByteBuffer.allocateDirect((5000 * 1024))

    private val successListener: VLSuccessUrlRequestCallbackListener = successListener

    private val errorListener: VLErrorUrlRequestCallbackListener = errorListener

    override fun onRedirectReceived(
        request: UrlRequest?,
        info: UrlResponseInfo?,
        newLocationUrl: String?
    ) {
        Log.i(TAG, "onRedirectReceived method called.")

        var shouldFollow = true

        if (shouldFollow) {
            request?.followRedirect()
        } else {
            request?.cancel()
        }
    }

    override fun onResponseStarted(
        request: UrlRequest?,
        info: UrlResponseInfo?
    ) {
        Log.i(TAG, "onResponseStarted method called.")
        val httpStatusCode = info?.httpStatusCode

        request?.read(buffer)

        var responseHeaders = info?.allHeaders
    }

    override fun onReadCompleted(
        request: UrlRequest?,
        info: UrlResponseInfo?,
        byteBuffer: ByteBuffer?
    ) {
        request?.read(buffer)

        Log.i(TAG, "onReadCompleted method called.")
    }

    private fun onSucceeded(
        request: UrlRequest?,
        info: UrlResponseInfo?,
        responseString: String?
    ) {
        val httpStatusCode = info?.httpStatusCode

        Log.i(TAG, info?.url)
        Log.i(TAG, String.format("Response Code: %d", httpStatusCode))
        Log.i(TAG, responseString)

        var responseData = JSONObject()

        try {
            responseData = JSONObject(responseString)
        }
        catch (e: JSONException) {
            Log.e(TAG, e.message)

            return
        }
        finally {

        }
        var statusMessage: String = ""

        when (httpStatusCode) {
            // 1xx: Informational (информационные):
            // 100 Continue («продолжай»)
            100 -> {
                statusMessage = "100 Continue («продолжай»)"
            }

            // 101 Switching Protocols («переключение протоколов»)
            101 -> {
                statusMessage = "101 Switching Protocols («переключение протоколов»)"
            }

            // 102 Processing («идёт обработка»)
            102 -> {
                statusMessage = "102 Processing («идёт обработка»)"
            }

            // 2xx: Success (успешно):
            // 200 OK («хорошо»)
            200 -> {
                statusMessage = "200 OK («хорошо»)"
            }

            // 201 Created («создано»)
            201 -> {
                statusMessage = "201 Created («создано»)"
            }

            // 202 Accepted («принято»)
            202 -> {
                statusMessage = "202 Accepted («принято»)"
            }

            // 203 Non-Authoritative Information («информация не авторитетна»)
            203 -> {
                statusMessage = "203 Non-Authoritative Information («информация не авторитетна»)"
            }

            // 204 No Content («нет содержимого»)
            204 -> {
                statusMessage = "204 No Content («нет содержимого»)"
            }

            // 205 Reset Content («сбросить содержимое»)
            205 -> {
                statusMessage = "205 Reset Content («сбросить содержимое»)"
            }

            // 206 Partial Content («частичное содержимое»)
            206 -> {
                statusMessage = "206 Partial Content («частичное содержимое»)"
            }

            // 207 Multi-Status («многостатусный»)
            207 -> {
                statusMessage = "207 Multi-Status («многостатусный»)"
            }

            // 208 Already Reported («уже сообщалось»)
            208 -> {
                statusMessage = "208 Already Reported («уже сообщалось»)"
            }

            // 226 IM Used («использовано IM»)
            226 -> {
                statusMessage = "226 IM Used («использовано IM»)"
            }

            // 3xx: Redirection (перенаправление):
            // 300 Multiple Choices («множество выборов»)
            300 -> {
                statusMessage = "300 Multiple Choices («множество выборов»)"
            }

            // 301 Moved Permanently («перемещено навсегда»)
            301 -> {
                statusMessage = "301 Moved Permanently («перемещено навсегда»)"
            }

            // 302 Moved Temporarily («перемещено временно»)
            // 302 Found («найдено»)
            302 -> {
                statusMessage = "302 Found («найдено»)"
            }

            // 303 See Other («смотреть другое»)
            303 -> {
                statusMessage = "303 See Other («смотреть другое»)"
            }

            // 304 Not Modified («не изменялось»)
            304 -> {
                statusMessage = "304 Not Modified («не изменялось»)"
            }

            // 305 Use Proxy («использовать прокси»)
            305 -> {
                statusMessage = "305 Use Proxy («использовать прокси»)"
            }

            // 306 — зарезервировано (код использовался только в ранних спецификациях)
            306 -> {
                statusMessage =
                    "306 — зарезервировано (код использовался только в ранних спецификациях)"
            }

            // 307 Temporary Redirect («временное перенаправление»)
            307 -> {
                statusMessage = "307 Temporary Redirect («временное перенаправление»)"
            }

            // 308 Permanent Redirect («постоянное перенаправление»)
            308 -> {
                statusMessage = "308 Permanent Redirect («постоянное перенаправление»)"
            }

            // 4xx: Client Error (ошибка клиента):
            // 400 Bad Request («плохой, неверный запрос»)
            400 -> {
                statusMessage = "400 Bad Request («плохой, неверный запрос»)"
            }

            // 401 Unauthorized («не авторизован (не представился)»)
            401 -> {
                statusMessage = "401 Unauthorized («не авторизован (не представился)»)"
            }

            // 402 Payment Required («необходима оплата»)
            402 -> {
                statusMessage = "402 Payment Required («необходима оплата»)"
            }

            // 403 Forbidden («запрещено (не уполномочен)»)
            403 -> {
                statusMessage = "403 Forbidden («запрещено (не уполномочен)»)"
            }

            // 404 Not Found («не найдено»)
            404 -> {
                statusMessage = "404 Not Found («не найдено»)"
            }

            // 405 Method Not Allowed («метод не поддерживается»)
            405 -> {
                statusMessage = "405 Method Not Allowed («метод не поддерживается»)"
            }

            // 406 Not Acceptable («неприемлемо»)
            406 -> {
                statusMessage = "406 Not Acceptable («неприемлемо»)"
            }

            // 407 Proxy Authentication Required («необходима аутентификация прокси»)
            407 -> {
                statusMessage =
                    "407 Proxy Authentication Required («необходима аутентификация прокси»)"
            }

            // 408 Request Timeout («истекло время ожидания»)
            408 -> {
                statusMessage = "408 Request Timeout («истекло время ожидания»)"
            }

            // 409 Conflict («конфликт»)
            409 -> {
                statusMessage = "409 Conflict («конфликт»)"
            }

            // 410 Gone («удалён»)
            410 -> {
                statusMessage = "410 Gone («удалён»)"
            }

            // 411 Length Required («необходима длина»)
            411 -> {
                statusMessage = "411 Length Required («необходима длина»)"
            }

            // 412 Precondition Failed («условие ложно»)
            412 -> {
                statusMessage = "412 Precondition Failed («условие ложно»)"
            }

            // 413 Payload Too Large («полезная нагрузка слишком велика»)
            413 -> {
                statusMessage = "413 Payload Too Large («полезная нагрузка слишком велика»)"
            }

            // 414 URI Too Long («URI слишком длинный»)
            414 -> {
                statusMessage = "414 URI Too Long («URI слишком длинный»)"
            }

            // 415 Unsupported Media Type («неподдерживаемый тип данных»)
            415 -> {
                statusMessage = "415 Unsupported Media Type («неподдерживаемый тип данных»)"
            }

            // 416 Range Not Satisfiable («диапазон не достижим»)
            416 -> {
                statusMessage = "416 Range Not Satisfiable («диапазон не достижим»)"
            }

            // 417 Expectation Failed («ожидание не удалось»)
            417 -> {
                statusMessage = "417 Expectation Failed («ожидание не удалось»)"
            }

            // 418 I’m a teapot («я — чайник»)
            418 -> {
                statusMessage = " 418 I’m a teapot («я — чайник»)"
            }

            // 419 Authentication Timeout (not in RFC 2616) («обычно ошибка проверки CSRF»)
            419 -> {
                statusMessage =
                    "419 Authentication Timeout (not in RFC 2616) («обычно ошибка проверки CSRF»)"
            }

            // 421 Misdirected Request
            421 -> {
                statusMessage = "421 Misdirected Request"
            }

            // 422 Unprocessable Entity («необрабатываемый экземпляр»)
            422 -> {
                statusMessage = "422 Unprocessable Entity («необрабатываемый экземпляр»)"
            }

            // 423 Locked («заблокировано»)
            423 -> {
                statusMessage = "423 Locked («заблокировано»)"
            }

            // 424 Failed Dependency («невыполненная зависимость»)
            424 -> {
                statusMessage = "424 Failed Dependency («невыполненная зависимость»)"
            }

            // 426 Upgrade Required («необходимо обновление»)
            426 -> {
                statusMessage = "426 Upgrade Required («необходимо обновление»)"
            }

            // 428 Precondition Required («необходимо предусловие»)
            428 -> {
                statusMessage = "428 Precondition Required («необходимо предусловие»)"
            }

            // 429 Too Many Requests («слишком много запросов»)
            429 -> {
                statusMessage = "429 Too Many Requests («слишком много запросов»)"
            }

            // 431 Request Header Fields Too Large («поля заголовка запроса слишком большие»)
            431 -> {
                statusMessage =
                    "431 Request Header Fields Too Large («поля заголовка запроса слишком большие»)"
            }

            // 449 Retry With («повторить с»)
            449 -> {
                statusMessage = "449 Retry With («повторить с»)"
            }

            // 451 Unavailable For Legal Reasons («недоступно по юридическим причинам»)
            451 -> {
                statusMessage =
                    "451 Unavailable For Legal Reasons («недоступно по юридическим причинам»)"
            }

            // 499 Client Closed Request (клиент закрыл соединение)
            499 -> {
                statusMessage = "499 Client Closed Request (клиент закрыл соединение)"
            }

            // 5xx: Server Error (ошибка сервера):
            // 500 Internal Server Error («внутренняя ошибка сервера»)
            500 -> {
                statusMessage = "500 Internal Server Error («внутренняя ошибка сервера»)"
            }

            // 501 Not Implemented («не реализовано»)
            501 -> {
                statusMessage = "501 Not Implemented («не реализовано»)"
            }

            // 502 Bad Gateway («плохой, ошибочный шлюз»)
            502 -> {
                statusMessage = "502 Bad Gateway («плохой, ошибочный шлюз»)"
            }

            // 503 Service Unavailable («сервис недоступен»)
            503 -> {
                statusMessage = "503 Service Unavailable («сервис недоступен»)"
            }

            // 504 Gateway Timeout («шлюз не отвечает»)
            504 -> {
                statusMessage = "504 Gateway Timeout («шлюз не отвечает»)"
            }

            // 505 HTTP Version Not Supported («версия HTTP не поддерживается»)
            505 -> {
                statusMessage = "505 HTTP Version Not Supported («версия HTTP не поддерживается»)"
            }

            // 506 Variant Also Negotiates («вариант тоже проводит согласование»)
            506 -> {
                statusMessage = "506 Variant Also Negotiates («вариант тоже проводит согласование»)"
            }

            // 507 Insufficient Storage («переполнение хранилища»)
            507 -> {
                statusMessage = "507 Insufficient Storage («переполнение хранилища»)"
            }

            // 508 Loop Detected («обнаружено бесконечное перенаправление»)
            508 -> {
                statusMessage = "508 Loop Detected («обнаружено бесконечное перенаправление»)"
            }

            // 509 Bandwidth Limit Exceeded («исчерпана пропускная ширина канала»)
            509 -> {
                statusMessage =
                    "509 Bandwidth Limit Exceeded («исчерпана пропускная ширина канала»)"
            }

            // 510 Not Extended («не расширено»)
            510 -> {
                statusMessage = "510 Not Extended («не расширено»)"
            }

            // 511 Network Authentication Required («требуется сетевая аутентификация»)
            511 -> {
                statusMessage =
                    "511 Network Authentication Required («требуется сетевая аутентификация»)"
            }

            // 520 Unknown Error («неизвестная ошибка»)
            520 -> {
                statusMessage = "520 Unknown Error («неизвестная ошибка»)"
            }

            // 521 Web Server Is Down («веб-сервер не работает»)
            521 -> {
                statusMessage = "521 Web Server Is Down («веб-сервер не работает»)"
            }

            // 522 Connection Timed Out («соединение не отвечает»)
            522 -> {
                statusMessage = "522 Connection Timed Out («соединение не отвечает»)"
            }

            // 523 Origin Is Unreachable («источник недоступен»)
            523 -> {
                statusMessage = "523 Origin Is Unreachable («источник недоступен»)"
            }

            // 524 A Timeout Occurred («время ожидания истекло»)
            524 -> {
                statusMessage = "524 A Timeout Occurred («время ожидания истекло»)"
            }

            // 525 SSL Handshake Failed («квитирование SSL не удалось»)
            525 -> {
                statusMessage = "525 SSL Handshake Failed («квитирование SSL не удалось»)"
            }

            // 526 Invalid SSL Certificate («недействительный сертификат SSL»)
            526 -> {
                statusMessage = "526 Invalid SSL Certificate («недействительный сертификат SSL»)"
            }

            else -> {

            }
        }

        when (httpStatusCode) {
            200, 201, 202, 203, 204, 205, 206, 207, 208, 226 -> {
                if (successListener != null) {
                    if (!responseData.isNull("success")) {
                        if (responseData.getBoolean("success")) {
                                successListener.SuccessDefaultHandler(responseData)
                        } else {
                            if (!responseData.isNull("errors")) {
                                val keys = responseData.getJSONObject("errors").keys()

                                statusMessage =
                                    responseData.getJSONObject("errors").getJSONArray(keys.next())
                                        .getString(0)
                            }

                                errorListener.ErrorDefaultHandler(
                                    statusMessage,
                                    Error(""),
                                    httpStatusCode,
                                    responseData
                                )

                        }
                    }
                }
            }
            else -> {
                if (errorListener != null) {
                    if (!responseData.isNull("message")) {
                        statusMessage = responseData.getString("message")
                    }

                    errorListener.ErrorDefaultHandler(statusMessage, Error(""), httpStatusCode!!, responseData)
                }
            }
        }
    }

    override fun onSucceeded(request: UrlRequest?, info: UrlResponseInfo?) {
        Log.i(TAG, "onSucceeded method called.")
        Log.i(TAG, info!!.allHeaders.toString())

        var byteArray = buffer.array()
        val emptyChar = 0
        var responseString = ""
        var uCount = 0

        responseString = String(byteArray, Charset.forName("UTF-8"))

        responseString = responseString.trim {
            val st = it.toInt()
            st == 0
        }

        onSucceeded(request, info, responseString)

        buffer.clear()
    }

    override fun onCanceled(request: UrlRequest?, info: UrlResponseInfo?) {
        request?.run {}
    }

    override fun onFailed(request: UrlRequest?, info: UrlResponseInfo?, error: CronetException?) {
        // The request has failed. If possible, handle the error.
        Log.e(TAG, "The request failed.", error)

        val allHeaders = info?.allHeaders

        info.hashCode()

        request?.apply {}
    }
}
