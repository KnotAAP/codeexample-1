
package com.vlc.slimbk.bk_base

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.view.ViewGroup
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.FragmentManager
import androidx.recyclerview.widget.RecyclerView
import com.vlc.slimbk.R

open class BehaviorNestedScroll(
    context: Context,
    attrs: AttributeSet?
) :
    CoordinatorLayout.Behavior<View>(),
    NestedScrollView.OnScrollChangeListener {

    var activity: BaseActivity? = null
    var fragmentManager: FragmentManager? = null

    var nestedScroll: NestedScrollView? = null

    private val TAG = "behavior"
    private var mContext: Context? = context

    private var isFirst = true

    init {

    }

    open fun initNestedScroll (activity: BaseActivity) {

    }

    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        activity = this.mContext as BaseActivity?

        return dependency.id == R.id.fragment_header
    }

    override fun onDependentViewChanged(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        if (isFirst) {
            activity = this.mContext as BaseActivity?

            activity?.let {
                initNestedScroll(it)
            }

            val fragmentList = parent.findViewById<RecyclerView>(R.id.fragment_list)

            nestedScroll = parent.findViewById<NestedScrollView>(R.id.nestedScroll)

            nestedScroll!!.setOnScrollChangeListener(this)

            var params = fragmentList.layoutParams as ViewGroup.MarginLayoutParams
            var paramsNested = nestedScroll!!.layoutParams

            paramsNested.height = parent.height

            nestedScroll!!.layoutParams = paramsNested
        }

        isFirst = false

        return true
    }

    override fun onNestedScroll(
        coordinatorLayout: CoordinatorLayout,
        child: View,
        target: View,
        dxConsumed: Int,
        dyConsumed: Int,
        dxUnconsumed: Int,
        dyUnconsumed: Int,
        type: Int
    ) {
        super.onNestedScroll(
            coordinatorLayout,
            child,
            target,
            dxConsumed,
            dyConsumed,
            dxUnconsumed,
            dyUnconsumed,
            type
        )
    }

    override fun onStartNestedScroll(
        coordinatorLayout: CoordinatorLayout,
        child: View,
        directTargetChild: View,
        target: View,
        axes: Int,
        type: Int
    ): Boolean {
        return super.onStartNestedScroll(
            coordinatorLayout,
            child,
            directTargetChild,
            target,
            axes,
            type
        )
    }

    override fun onNestedPreFling(
        coordinatorLayout: CoordinatorLayout,
        child: View,
        target: View,
        velocityX: Float,
        velocityY: Float
    ): Boolean {
        return super.onNestedPreFling(coordinatorLayout, child, target, velocityX, velocityY)
    }

    override fun onNestedScrollAccepted(
        coordinatorLayout: CoordinatorLayout,
        child: View,
        directTargetChild: View,
        target: View,
        axes: Int,
        type: Int
    ) {
        super.onNestedScrollAccepted(
            coordinatorLayout,
            child,
            directTargetChild,
            target,
            axes,
            type
        )
    }

    override fun onNestedPreScroll(
        coordinatorLayout: CoordinatorLayout,
        child: View,
        target: View,
        dx: Int,
        dy: Int,
        consumed: IntArray,
        type: Int
    ) {
        super.onNestedPreScroll(coordinatorLayout, child, target, dx, dy, consumed, type)
    }

    override fun onTouchEvent(parent: CoordinatorLayout, child: View, ev: MotionEvent): Boolean {
        return super.onTouchEvent(parent, child, ev)
    }

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {

    }
}
