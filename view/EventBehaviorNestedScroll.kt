package com.vlc.slimbk.bk_base

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.NestedScrollView
import com.vlc.slimbk.R
import com.vlc.slimbk.fragments.event.EventMainHeaderFragment

class EventBehaviorNestedScroll(
    context: Context,
    attrs: AttributeSet?
): BehaviorNestedScroll(context, attrs) {
    var fragmentHeader: EventMainHeaderFragment? = null

    init {

    }

    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        val flag = dependency.id == R.id.fragment_header

        return flag
    }

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {
        fragmentHeader = activity!!.fragmentManager!!.findFragmentById(R.id.fragment_header) as EventMainHeaderFragment?
        fragmentHeader!!.initHeights()

        val d = fragmentHeader!!.fullHeight - fragmentHeader!!.slimHeight
        val alpha = 1.0 - (scrollY.toDouble() / d.toDouble())

        fragmentHeader!!.setVisibleHeaderSwitchState(alpha)
    }
}
