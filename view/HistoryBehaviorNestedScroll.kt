package com.vlc.slimbk.bk_base

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.NestedScrollView
import com.vlc.slimbk.R

class HistoryBehaviorNestedScroll(
    context: Context,
    attrs: AttributeSet?
): BehaviorNestedScroll(context, attrs) {
    var historyHeader: View? = null
    var amountHeader: View? = null

    init {

    }

    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        val flag = dependency.id == R.id.header_view

        return flag
    }

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {
        historyHeader = activity!!.findViewById(R.id.header_view)
        amountHeader = activity!!.findViewById(R.id.amount_header_view)

        if(amountHeader!!.height >= scrollY.toFloat()) {
            historyHeader!!.y = -scrollY.toFloat()
        } else {
            historyHeader!!.y = (-amountHeader!!.height).toFloat()
        }
    }
}
