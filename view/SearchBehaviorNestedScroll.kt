package com.vlc.slimbk.bk_base

import android.content.Context
import android.util.AttributeSet
import android.view.View
import androidx.coordinatorlayout.widget.CoordinatorLayout
import androidx.core.widget.NestedScrollView
import com.vlc.slimbk.R
import com.vlc.slimbk.activities.tournaments.TournamentsActivity

class SearchBehaviorNestedScroll (
    context: Context,
    attrs: AttributeSet?
): BehaviorNestedScroll(context, attrs) {
    var searchHeader: View? = null

    init {

    }

    override fun initNestedScroll (activity: BaseActivity) {
        val a = activity as TournamentsActivity

        a.searchBehaviorNestedScroll = this

        searchHeader = activity!!.findViewById(R.id.header_view)
    }

    override fun layoutDependsOn(
        parent: CoordinatorLayout,
        child: View,
        dependency: View
    ): Boolean {
        val flag = dependency.id == R.id.header_view

        return flag
    }

    override fun onScrollChange(
        v: NestedScrollView?,
        scrollX: Int,
        scrollY: Int,
        oldScrollX: Int,
        oldScrollY: Int
    ) {


        searchHeader!!.y = -scrollY.toFloat()

        if(0 >= scrollY) {

        } else {

        }
    }

    fun setVisibleHeader (visible: Boolean) {
        if (visible) {
            searchHeader!!.visibility = View.VISIBLE
        } else {
            searchHeader!!.visibility = View.GONE
        }
    }
}