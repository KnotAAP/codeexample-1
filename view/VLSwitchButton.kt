package com.vlc.slimbk.vl_controls

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.util.AttributeSet
import android.view.View
import android.widget.ImageView
import android.widget.LinearLayout
import com.vlc.slimbk.R

open class VLSwitchButton(context: Context?, attrs: AttributeSet?) :
    LinearLayout(context, attrs) {

    interface OnClickListener {
        fun onClick(check: Boolean, view: View) {}
    }

    var imageView: ImageView? = null

    var onBitmap: Bitmap? = null
    var offBitmap: Bitmap? = null

    var check = false

    var listenerOn: OnClickListener? = null

    init {
        orientation = VERTICAL
        layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )

        this.setOnClickListener {
            onClickAction()
        }

        imageView = ImageView(context)

        imageView?.layoutParams = LayoutParams(
            LayoutParams.MATCH_PARENT,
            LayoutParams.MATCH_PARENT
        )

        initBitmaps()

        setSwitch(check)

        addView(imageView)
    }

    open fun initBitmaps() {
        val on = BitmapFactory.decodeResource(resources, R.drawable.check_on)
        val off = BitmapFactory.decodeResource(resources, R.drawable.check_off)

        setImages(on, off)
    }

    private fun checkSwitch () {
        check = !check

        setSwitch(check)
    }

    fun setSwitch(switchOn: Boolean) {
        check = switchOn

        if (switchOn) {
            imageView?.setImageBitmap(onBitmap)
        } else {
            imageView?.setImageBitmap(offBitmap)
        }
    }

    fun setImages(on: Bitmap, off: Bitmap){
        onBitmap = on
        offBitmap = off
    }

    private fun onClickAction() {
        checkSwitch ()

        if (listenerOn != null) {
            listenerOn?.onClick(check, this)
        }
    }
}